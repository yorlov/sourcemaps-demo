const WrmPlugin = require('atlassian-webresource-webpack-plugin');
const path = require('path');

const ROOT_DIR = path.resolve(__dirname);
const FRONTEND_DIR = path.join(ROOT_DIR, 'src', 'main', 'frontend')
const OUTPUT_DIR = path.join(ROOT_DIR, 'target', 'classes');
const DESCRIPTORS_PATH = path.join(OUTPUT_DIR, 'META-INF', 'plugin-descriptors', 'bundles.xml')

module.exports = {
    devtool: 'source-map',
    target: ['es5'],
    context: FRONTEND_DIR,
    entry: {
        feature: path.join(FRONTEND_DIR, 'feature.ts'),
    },
    output: {
        filename: '[name].js',
        libraryTarget: 'amd',
        library: 'bitbucket-plugin-test/[name]',
        path: OUTPUT_DIR
    },
    plugins: [
        new WrmPlugin({
            pluginKey: 'com.stiltsoft.bitbucket.test',
            transformationMap: {
                js: ['jsI18n']
            },
            contextMap: {
                'feature': ['atl.general']
            },
            xmlDescriptors: DESCRIPTORS_PATH
        })
    ],
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            }
        ],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    }
};
